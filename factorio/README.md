I want to build an accurate representation of factorio crafting in prolog,
starting by parsing the lua-table-formatted list of recipes exported by
factorio. Eventually I want to be able to ask interesting questions like how
many machines or belts of a certain type are required to meet some throughput
goal.
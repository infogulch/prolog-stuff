/c local list = {}
for _, recipe in pairs(game.player.force.recipes) do 
    if recipe.enabled then
        list[#list+1] = {order = recipe.order, name = recipe.name, energy = recipe.energy, category = recipe.category, group = recipe.group.name, subgroup = recipe.subgroup.name, ingredients = recipe.ingredients, products = recipe.products}
    end
end
game.write_file("recipes.lua", serpent.block(list) .. "\n", true)

-- replace `(\w+) = with `"$1":`


PTTP is a theorem-prover for the first-order predicate calculus
that uses the model elimination inference procedure and iterative
deepening search.  Input formulas are compiled by PTTP for direct
execution by Prolog, so individual inference operations are fast.


src/pttp.pl
src/pttp-dalit.pl
	contains the source code for PTTP
examples/pttp-examples.pl
	contains some example problems
examples/pttp-examples.typescript
	transcript of execution of example problems


doc/pttp.html
doc/pttp-dalit.html
	presents the source more readably
doc/pttp-examples.html
	presents the examples more readably
doc/stickel-pttp-jar.ps.gz
	Journal of Automated Reasoning article describes
	the principles of PTTP and its implementation in Lisp
doc/stickel-pttp-tr.ps.gz
	SRI Artificial Intelligence Center technical report describes
	the principles of PTTP and its implementation in Prolog
	(similar but not identical to this one)
doc/tptp-results-for-pttp.txt
doc/tptp-results-for-pttp-dalit.txt
	results for PTTP on the TPTP problem set


to use PTTP,
1. compile and load pttp.pl into Prolog
2. use the 'pttp' predicate to compile formulas
3. use the 'prove' predicate to do a proof


for example,
pl
['pttp.pl'].
pttp((a(1),
      (not_a(X) ; b(X)))).
prove(b(Y)).


PTTP uses depth-first iterative-deepening search on the proof-length
parameter (the size of the proof tree).  Another commonly used
parameter is the proof-depth parameter (the height of the proof tree).
PTTP can be modified to use the "D_Alit" proof-depth parameter by
loading pttp-dalit.pl after pttp.pl.


The 'timed_call' predicate uses the 'statistics' predicate to get
run-time in seconds.  The 'statistics' predicate differs between
Prolog implementations, so 'timed_call' may need to be rewritten
slightly to work on different Prolog implementations.


PTTP was written by
Mark E. Stickel
Artificial Intelligence Center
SRI International
stickel@ai.sri.com
http://www.ai.sri.com/~stickel
